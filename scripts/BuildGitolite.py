#!/usr/bin/python3
# Copyright 2016 Boudhayan Gupta <bgupta@kde.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. Neither the name of KDE e.V. (or its successor approved by the
#    membership of KDE e.V.) nor the names of its contributors may be used
#    to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import os
import sys
import io
import shutil
import re
import yaml

def ProcessDirectory(buf, path):
    # enter the directory and start processing
    os.chdir(path)

    # get the repo path and start working
    if os.path.isfile("metadata.yaml"):
        meta = None
        with open("metadata.yaml") as f:
            meta = yaml.load(f)

        if meta["hasrepo"]:
            repopath = meta["repopath"]
            name = repopath.split("/")[-1]
            desc = meta["description"]
            desc = re.sub('\!.+\!', "", desc).strip()
            if "\n" in desc:
                desc = desc.split("\n")[0].strip()

            buf.write("repo {0}\n".format(repopath))
            buf.write("    desc = \"{1}\"\n".format(repopath, desc))

            if repopath.endswith("-history"):
                buf.write("    R    = @all\n")
            elif not "/" in repopath:
                buf.write("    RWCD = @all\n")
            elif repopath.startswith("sysadmin"):
                buf.write("    RWCD = @sysadmins\n")
                buf.write("    R    = @all\n")
            elif repopath.startswith("websites"):
                buf.write("    RWCD = @www @sysadmins\n")
                buf.write("    R    = @all\n")
            elif repopath.startswith("other"):
                buf.write("    RWCD = @sysadmins\n")
                buf.write("    RWC  = @all\n")
            else:
                print("error: unrecognized repo path: {0}".format(repopath))
                sys.exit(1)

            buf.write("\n")

    # recurse into subdirectories
    for entry in sorted(os.listdir(".")):
        if os.path.isdir(entry):
            ProcessDirectory(buf, entry)

    # done
    os.chdir("..")

if __name__ == "__main__":

    # build the buffer
    buf = io.StringIO()

    # walk through the project hierarchy
    os.chdir("projects")
    for entry in sorted(os.listdir(".")):
        ProcessDirectory(buf, entry)
    os.chdir("..")

    # write it to a file
    fname = "output/gitolite.conf"
    with open(fname, "w", encoding = "utf-8") as f:
        buf.seek(0)
        shutil.copyfileobj(buf, f)
