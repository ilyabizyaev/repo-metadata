#!/usr/bin/python3
# Copyright 2016 Boudhayan Gupta <bgupta@kde.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. Neither the name of KDE e.V. (or its successor approved by the
#    membership of KDE e.V.) nor the names of its contributors may be used
#    to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import os
import yaml
import lxml.etree

try:
    import simplejson as json
except ImportError:
    import json

def ProcessRepo(el):
    repoActive = False
    repoPath = None

    for child in el:
        tag = child.tag.lower()
        if tag == "url" and child.attrib["protocol"] ==  "ssh":
            repoPath = child.text.strip().split(":")[1]
        elif tag == "active" and child.text.strip().lower() == "true":
            repoActive = True

    return (repoActive, repoPath)

def ProcessI18NBranches(el):
    branches = []
    for child in el:
        if child.tag.lower() == "branch":
            try:
                branches.append((child.attrib["i18n"], child.text.strip()))
            except:
                continue
    branches = dict(branches)
    return json.dumps(branches)

def ProcessMetadata(el):
    metaData = {
        "type" : el.tag.lower(),
        "name" : None,
        "description" : None,
        "projectpath" : None,
        "hasrepo" : False,
        "repopath" : None,
        "repoactive" : False,
        "icon" : None,
        "members" : []
    }

    for child in el:
        tag = child.tag.lower()

        if tag == "name":
            try:
                metaData["name"] = child.text.strip()
            except:
                continue
        elif tag == "description":
            try:
                metaData["description"] = child.text.strip()
            except:
                continue
        elif tag == "path":
            try:
                metaData["projectpath"] = child.text.strip().lower()
            except:
                continue
        elif tag == "icon":
            try:
                metaData["icon"] = child.text.strip().lower()
            except:
                continue
        elif tag == "member":
            data = { "username" : child.attrib["username"], "displayname" : child.text.strip() }
            metaData["members"].append(data)
        elif tag == "repo":
            metaData["hasrepo"] = True
            metaData["repoactive"], metaData["repopath"] = ProcessRepo(child)

    return yaml.dump(metaData, default_flow_style = False)

def RecursiveVisit(el):
    for child in el:
        tag = child.tag.lower()

        if tag in ("component", "module", "project"):
            name = child.attrib["identifier"]
            if not os.path.isdir(name):
                os.mkdir(name)
            os.chdir(name)

            with open("metadata.yaml", "w") as f:
                f.write(ProcessMetadata(child))
            RecursiveVisit(child)

            os.chdir("..")
        elif tag == "repo":
            with open("i18n.json", "w") as f:
                f.write(ProcessI18NBranches(child))

if __name__ == "__main__":
    root = lxml.etree.ElementTree().parse("masters/kde_projects.xml")
    os.chdir("projects")
    RecursiveVisit(root)
    os.chdir("..")
