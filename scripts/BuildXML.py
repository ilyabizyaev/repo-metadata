#!/usr/bin/python3
# Copyright 2016 Boudhayan Gupta <bgupta@kde.org>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. Neither the name of KDE e.V. (or its successor approved by the
#    membership of KDE e.V.) nor the names of its contributors may be used
#    to endorse or promote products derived from this software without
#    specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import os
import yaml
import git
import copy
import collections
import fnmatch
import lxml.etree

try:
    import simplejson as json
except ImportError:
    import json

# load all configuration data

COMMONCFG = None
with open("config/common.json") as f:
    COMMONCFG = json.load(f)

I18NDEFAULTS = None
with open("config/i18n_defaults.json") as f:
    I18NDEFAULTS = json.load(f, object_pairs_hook = collections.OrderedDict)

UGITREPO = None
with open("config/urls_gitrepo.json") as f:
    UGITREPO = json.load(f)

UWEBACCESS = None
with open("config/urls_webaccess.json") as f:
    UWEBACCESS = json.load(f)

# done loading configuration data

def GetI18NRules(path):
    for k in I18NDEFAULTS.keys():
        if (fnmatch.fnmatch(path, k)):
            return copy.deepcopy(I18NDEFAULTS[k])
    return { "trunk" : "none", "stable" : "none", "trunk_kf5" : "none", "stable_kf5" : "none" }

def ProcessDirectory(el, path):
    # enter the directory and start processing
    os.chdir(path)

    # process metadata first
    projectpath = None
    repopath = None

    if os.path.isfile("metadata.yaml"):
        meta = None
        with open("metadata.yaml") as f:
            meta = yaml.load(f)

        # read in the project and repository paths
        projectpath = meta["projectpath"]
        if meta["hasrepo"]:
            repopath = meta["repopath"]

        elem = lxml.etree.SubElement(el, meta["type"])
        elem.set("identifier", path)

        # build the metadata part of the xml
        name = lxml.etree.SubElement(elem, "name")
        name.text = meta["name"]
        desc = lxml.etree.SubElement(elem, "description")
        desc.text = meta["description"]
        icon = lxml.etree.SubElement(elem, "icon")
        icon.text = meta["icon"]
        path = lxml.etree.SubElement(elem, "path")
        path.text = meta["projectpath"]

        web = lxml.etree.SubElement(elem, "web")
        if repopath:
            web.text = UWEBACCESS["gitweb"].format(repopath)

        for member in meta["members"]:
            mel = lxml.etree.SubElement(elem, "member")
            mel.set("username", member["username"])
            mel.text = member["displayname"]

        # process repository data next
        if meta["hasrepo"]:
            repoel = lxml.etree.SubElement(elem, "repo")

            active = lxml.etree.SubElement(repoel, "active")
            active.text = str(meta["repoactive"]).lower()

            for k in UWEBACCESS.keys():
                wel = lxml.etree.SubElement(repoel, "web")
                wel.set("type", k)
                wel.text = UWEBACCESS[k].format(repopath)

            for k in UGITREPO.keys():
                uel = lxml.etree.SubElement(repoel, "url")
                uel.set("protocol", k)
                uel.set("access", UGITREPO[k]["access"])
                uel.text = UGITREPO[k]["url"].format(repopath)

            gitrepopath = os.path.join(COMMONCFG["repostore"], repopath) + ".git"
            try:
                gitrepo = git.Repo(gitrepopath)
                for branch in gitrepo.branches:
                    bel = lxml.etree.SubElement(repoel, "branch")
                    bel.text = branch.name
            except Exception as exc:
                print("error: no git repository at {0}. unable to get branches. exception {1}".format(gitrepopath, exc))

            # add in the i18n rules
            i18n_rules = GetI18NRules(projectpath)

            # get the i18n overrides
            i18n_ovr = {}
            if os.path.isfile("i18n.json"):
                with open("i18n.json") as f:
                    i18n_ovr = json.load(f)

            for k in i18n_ovr.keys():
                i18n_rules[k] = i18n_ovr[k]

            # write the i18n branches
            for b in i18n_rules.keys():
                bel = lxml.etree.SubElement(repoel, "branch")
                bel.set("i18n", b)
                bel.text = i18n_rules[b]

    # recurse into subdirectories
    for entry in os.listdir("."):
        if os.path.isdir(entry):
            ProcessDirectory(elem, entry)

    # done
    os.chdir("..")

if __name__ == "__main__":

    # build the root element
    root = lxml.etree.Element("kdeprojects")
    root.set("version", "1")

    # walk through the project hierarchy
    os.chdir("projects")
    for entry in os.listdir("."):
        ProcessDirectory(root, entry)
    os.chdir("..")

    # write it to a file
    fname = "output/kde_projects.xml"
    tree = lxml.etree.ElementTree(root)
    tree.write(fname, pretty_print = True, xml_declaration = True, encoding = "utf-8")
